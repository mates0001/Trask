#!/usr/bin/env python
"""
    REST Class

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
import requests
from os import path
from time import sleep
from requests.adapters import HTTPAdapter
from API_global_config import logger


# ======================================================================================================================
# REST class
# ======================================================================================================================
class REST:
    """
    REST class
    __init__        : Init
    connect         : Connect
    get_data        : Get data
    """

    def __init__(self, rest_initials: iter):
        """
        Init

        :param: rest_initials: iter
        """
        self.url, self.headers, self.data, = None, None, dict()
        self.rest_initials, self.basename = rest_initials, path.basename(__file__)

        self.session = requests.sessions.Session()
        self.session.mount(rest_initials.get("adapter"), HTTPAdapter(pool_maxsize=rest_initials.get("pool_maxsize")))

    def connect(self, payload: iter or None, method: str) -> None:
        """
        Connect

        :param: payload: iter or None
        :param: method: str
        """
        # GET, POST or PATCH method
        if not payload and method == "GET":
            request_action = self.session.get

        elif payload and method == "POST":
            request_action = self.session.post

        elif payload and method == "PATCH":
            request_action = self.session.patch

        else:
            message = f"{self.basename}, error: GET, POST, PATCH method & payload mismatch"
            logger.error(message)

            raise Exception(message)

        # If debug level is set
        message = f"{self.basename}, url: {self.url}, headers: {self.headers}, payload: {payload}"
        logger.debug(message)

        response, success, retries, max_retries = None, False, 0, self.rest_initials.get("attempts")
        while retries < max_retries and success is False:
            retries += 1

            try:
                success = True

                response = request_action(self.url, headers=self.headers, data=payload,
                                          timeout=self.rest_initials.get("timeout"))

                self.data = response.json()

            except Exception as err:
                success = False

                if retries < max_retries:
                    logger.warning(f"{self.basename}, ATTEMPT {retries} - error: {err}")

                else:
                    message = f"{self.basename}, error: {err}"
                    logger.error(message)

                    raise Exception(message)

                sleep(self.rest_initials.get("timeout"))

        self.session.close()

        # HTTP not 200 OK
        if response.status_code != 200:
            message = f"{self.basename}, error: HTTP {response.status_code}"
            logger.error(message)

            raise Exception(message)

        # HTTP 200 OK but empty response, proxy etc
        if method == "GET" and not self.data:
            message = f"{self.basename}, error: Empty data received"
            logger.error(message)

            raise Exception(message)

    @property
    def get_data(self) -> dict or list:
        """
        Get data

        :return: self.data: dict
        """
        return self.data


# ======================================================================================================================
# END
# ======================================================================================================================
