#!/usr/bin/env python
"""
    Json Response

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
from flask import jsonify
#
from API_global_config import logger


# ======================================================================================================================
# Json Response
# ======================================================================================================================
def json_response(response: dict, code: int or None) -> (iter, int):
    """
    Json Response
        Convert dictionary to JSON and check/add missing error/response parts

    :param: response: dict
    :param: code: int

    :return: response: json, int
    """
    _response = dict()
    _response["error"] = response.get("error")
    _response["response"] = response.get("response")

    # Server error in case of error and code not given
    if response.get("error") and not code:
        code = 500

    # Logger type change
    if response.get("error"):
        logger.error(f"Response: {_response}")

    else:
        logger.info(f"Response: {_response}")

    return jsonify(_response), code


# ======================================================================================================================
# End
# ======================================================================================================================
