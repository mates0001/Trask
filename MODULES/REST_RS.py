#!/usr/bin/env python
"""
    Random Server class

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
from MODULES.REST import REST
from API_global_config import rest_rs, logger


# ======================================================================================================================
# Random Server class
# ======================================================================================================================
class RS(REST):
    """
    Random Server class

    __init__    : Implemented with Super in order to pass rest_credentials
    vip_in_time : VIP person coordinates in time
    """

    def __init__(self) -> None:
        """
        __init__
            Implemented with Super in order to pass rest_credentials
        """
        super().__init__(rest_initials=rest_rs)

    def vip_in_time(self, point_in_time: int) -> None:
        """
        VIP person coordinates in time

        :param: point_in_time: int
        """

        method, self.headers = 'GET', None
        self.url = f"{rest_rs.get('adapter')}{rest_rs.get('host')}:{rest_rs.get('port')}/v1/coords/{point_in_time}"

        logger.info(f"{self.basename}: method {method}, {self.url}")

        self.connect(payload=None, method=method)


# ======================================================================================================================
# End
# ======================================================================================================================
