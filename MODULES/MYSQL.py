#!/usr/bin/env Python
"""
    MySQL Class

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
import pymysql
from os import path
from time import sleep
from API_global_config import logger


# ======================================================================================================================
# MySQL class
# ======================================================================================================================
class MySQL:
    """
    MySQL class
    __init__                    : Init
    connect                     : Connect to DB
    get_data                    : Return data
    """

    def __init__(self, mysql_initials: iter, autocommit: bool) -> None:
        """
        Init

        :param: mysql_initials: iter
        :param: autocommit: bool
            In case of bigger amount of the data, autocommit should be set to True:
                DB table not exclusively locked for significant amount of time!
        """
        self.basename, self.autocommit, self.mysql_initials = path.basename(__file__), autocommit, mysql_initials
        self.db, self.cursor, self.data = None, None, list()

        self.connect()

    def connect(self) -> None:
        """
        Connect
        """
        success, retries, max_retries = False, 0, 3

        while retries < max_retries and success is False:
            retries += 1

            try:
                success = True
                self.db = pymysql.connect(host=self.mysql_initials.get('host'), port=self.mysql_initials.get('port'),
                                          user=self.mysql_initials.get('usr'), password=self.mysql_initials.get('pwd'),
                                          database=self.mysql_initials.get('database'), autocommit=self.autocommit,
                                          connect_timeout=self.mysql_initials.get('timeout'),
                                          cursorclass=pymysql.cursors.DictCursor)

                self.cursor = self.db.cursor()

            except Exception as err:
                success = False

                if retries < max_retries:
                    logger.warning(f'{self.basename}, method: connect, ATTEMPT {retries} - error: {err}')

                else:
                    message = f'{self.basename}, method: connect, error: {err}'
                    logger.error(message)

                    raise ConnectionError(message)

                sleep(self.mysql_initials.get('timeout'))

    def commit(self) -> None:
        """
        Commit & close
        """
        self.cursor.close()
        self.db.commit()
        self.db.close()

    def error(self) -> None:
        """
        Rollback & close
        """
        self.cursor.close()
        self.db.rollback()
        self.db.close()

    @property
    def get_data(self) -> list or dict:
        """
        Get data

        :return: self.data: list or dict
        """
        return self.data


# ======================================================================================================================
# End
# ======================================================================================================================
