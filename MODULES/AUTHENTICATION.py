#!/usr/bin/env python
"""
    AUTHENTICATION module

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
from os import path
from flask_basic_roles import BasicRoleAuth
from MODULES.MYSQL_USER import USER
from API_global_config import logger


# ======================================================================================================================
# Class AUTHENTICATION
# ======================================================================================================================
class AUTHENTICATION:
    """
    Class AUTHENTICATION
    __init__:   BasicRoleAuth init
    add_role:   Add role to user
    """

    def __init__(self):
        """
        Init
        """
        self.basename, self.basic_role, self.user_data = path.basename(__file__), BasicRoleAuth(), None

        try:
            user = USER()
            user.select_users()
            self.user_data = user.get_data

        except Exception as err:
            message = f'{self.basename}, method: Init, MySQL select user, error -> exit: {err}'
            logger.error(message)

            exit(99)

        self.add_role()

    def add_role(self):
        """
        Add role to user
        """
        for element in self.user_data:
            self.basic_role.add_user(password=element.get('password'),
                                     user=element.get('user'),
                                     roles=element.get('role'))

    @property
    def get_role(self):
        """
        :return: dictionary
        """
        return self.basic_role


# ======================================================================================================================
# End
# ======================================================================================================================
