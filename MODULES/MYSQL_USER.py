#!/usr/bin/env Python
"""
    MySQL User Class

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
from MODULES.MYSQL import MySQL
from API_global_config import logger, mysql


# ======================================================================================================================
# User class
# ======================================================================================================================
class USER(MySQL):
    """
    USER class

    __init__            : Implemented with Super in order to pass mysql_initials & autocommit
    select_users        : Select users
    """

    def __init__(self) -> None:
        """
        __init__
            Implemented with Super in order to pass mysql_initials & autocommit
        """
        super().__init__(mysql_initials=mysql, autocommit=True)

    def select_users(self) -> None:
        """
        Select users
        """
        sql_statement = 'SELECT user, password, role FROM users'

        try:
            self.cursor.execute(sql_statement)
            self.data = self.cursor.fetchall()

        except Exception as err:
            self.error()

            message = f'{self.basename}, method: select_users, error: {err}'
            logger.error(message)

            raise Exception(message)


# ======================================================================================================================
# End
# ======================================================================================================================
