#!/usr/bin/env python
"""
    API Service Map
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
from os import path as os_path
from flask import Flask
from waitress import serve
from API_global_config import api, logger
from MODULES.AUTHENTICATION import AUTHENTICATION
from MODULES.json_response import json_response
from FLOWS.HEALTH.LIVENESS.liveness import liveness
from FLOWS.HEALTH.READINESS.readiness import readiness
from FLOWS.V1.NOW.now import now
from FLOWS.V1.VIP.vip import vip


# ======================================================================================================================
# App & Authentication
# ======================================================================================================================
app = Flask(__name__)
basic_role = AUTHENTICATION().get_role


# ======================================================================================================================
# 404 NOT FOUND
# ======================================================================================================================
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def not_found(path) -> (iter, int):
    """
    Not found

    :param: path: iter
    :return: get_json_response: iter, int
    """
    return json_response(response={'error': f"Not Found: {path}"}, code=404)


# ======================================================================================================================
# HEALTH
# ======================================================================================================================
@app.route('/health/liveness', methods=['GET'])
def health_liveness() -> (iter, int):
    """
    Route: /health/liveness

    :return: get_json_response: iter, int
    """
    response, code = liveness()

    return json_response(response=response, code=code)


@app.route('/health/readiness', methods=['GET'])
def health_readiness() -> (iter, int):
    """
    Route: /health/readiness

    :return: get_json_response: iter, int
    """
    response, code = readiness()

    return json_response(response=response, code=code)


# ======================================================================================================================
# V1
# ======================================================================================================================
@app.route('/v1/now', methods=['GET'])
@basic_role.require(roles={'GET': ('user', 'vip')})
def v1_now() -> (iter, int):
    """
    Route: /v1/now

    :return: get_json_response: iter, int
    """
    response, code = now()

    return json_response(response=response, code=code)


@app.route('/v1/VIP/<int:point_in_time>', methods=['GET'])
@basic_role.require(roles={'GET': 'vip'})
def v1_vip(point_in_time: int) -> (iter, int):
    """
    Route: /v1/VIP/<point_in_time>

    :param: point_in_time: int
    :return: get_json_response: iter, int
    """
    response, code = vip(point_in_time=point_in_time)

    return json_response(response=response, code=code)


# ======================================================================================================================
# Call API
# ======================================================================================================================
if __name__ == '__main__':
    basename = os_path.basename(__file__)

    logger.info(f"{basename}: Server starting on {api.get('host')}:{api.get('port')}")

    try:
        serve(app, listen=f"{api.get('host')}:{api.get('port')}")

    except Exception as err:
        logger.error(f'{basename}: {str(err)}')

        exit(99)


# ======================================================================================================================
# End
# ======================================================================================================================
