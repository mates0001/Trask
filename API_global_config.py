#!/usr/bin/env python
"""
    API config

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
import sys
import logging
from os import environ


# ======================================================================================================================
# Constants
# ======================================================================================================================


# ======================================================================================================================
# MySQL
# ======================================================================================================================
mysql_host = '0.0.0.0' if not environ.get('MYSQL_HOST') else environ.get('MYSQL_HOST')
mysql_port = 3306 if not environ.get('MYSQL_PORT') else int(environ.get('MYSQL_PORT'))
mysql_timeout = 10 if not environ.get('MYSQL_TIMEOUT') else int(environ.get('MYSQL_TIMEOUT'))
mysql_usr = 'root' if not environ.get('MYSQL_USR') else environ.get('MYSQL_USR')
mysql_pwd = 'rootpassword' if not environ.get('MYSQL_PWD') else environ.get('MYSQL_PWD')
mysql_db = 'exam' if not environ.get('MYSQL_DB') else environ.get('MYSQL_DB')

mysql = {"host": mysql_host, "port": mysql_port, "timeout": mysql_timeout, "usr": mysql_usr, "pwd": mysql_pwd,
         "database": mysql_db}


# ======================================================================================================================
# REST Random Server
# ======================================================================================================================
rest_rs_host = '0.0.0.0' if not environ.get('REST_RS_HOST') else environ.get('REST_RS_HOST')
rest_rs_port = 8088 if not environ.get('REST_RS_PORT') else int(environ.get('REST_RS_PORT'))
rest_rs_adapter = 'http://' if not environ.get('REST_RS_HTTPS') else 'https://'
rest_rs_timeout = 5 if not environ.get('REST_RS_TIMEOUT') else int(environ.get('REST_RS_TIMEOUT'))
rest_rs_attempts = 1 if not environ.get('REST_RS_ATTEMPTS') else environ.get('REST_RS_ATTEMPTS')  # in order to FailFast
rest_rs_pool_maxsize = 50 if not environ.get('REST_RS_POOL_MAXSIZE') else int(environ.get('REST_RS_POOL_MAXSIZE'))

rest_rs = {"host": rest_rs_host, "port": rest_rs_port, "adapter": rest_rs_adapter, "timeout": rest_rs_timeout,
           "attempts": rest_rs_attempts, "pool_maxsize": rest_rs_pool_maxsize}


# ======================================================================================================================
# Flask: WSGI
# ======================================================================================================================
api_host = '0.0.0.0' if not environ.get('API_HOST') else environ.get('API_HOST')
api_port = '9999' if not environ.get('API_PORT') else environ.get('API_PORT')

api = {'host': api_host, 'port': int(api_port)}


# ======================================================================================================================
# Logging
# ======================================================================================================================
log_level = logging.INFO if not environ.get('DEBUG') else logging.DEBUG
logging.basicConfig(level=log_level, stream=sys.stdout, format='%(asctime)s  %(levelname)-10s  %(message)s',
                    datefmt="%Y-%m-%d-%H-%M-%S")
logger = logging.getLogger(__name__)


# ======================================================================================================================
# End
# ======================================================================================================================
