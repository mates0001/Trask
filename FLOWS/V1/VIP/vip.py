#!/usr/bin/env python
"""
    FLOW VIP

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
from os import path
from MODULES.REST_RS import RS


# ======================================================================================================================
# VIP
# ======================================================================================================================
def vip(point_in_time: int) -> (dict, int):
    """
    VIP person coordinates in time

    Get the data from Random Server

    :param: point_in_time: int
    :return: dict, int
    """
    response, basename = dict(), path.basename(__file__)

    # ==================================================================================================================
    # Get the data from Random Server
    # ==================================================================================================================
    try:
        rs = RS()
        rs.vip_in_time(point_in_time=point_in_time)
        data = rs.get_data

    except Exception as err:
        response["error"] = f'{basename}: Get the data from Random Server: {str(err)}'

        return response, 500

    response["response"] = {"source": "vip-db",
                            "gpsCoords": {"lat": data.get("latitude"),
                                          "long": data.get("longitude")}}

    return response, 200


# ======================================================================================================================
# End
# ======================================================================================================================
