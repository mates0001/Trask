#!/usr/bin/env python
"""
    FLOW readiness

    Author: Ludek Matousek
"""


# ======================================================================================================================
# Import modules
# ======================================================================================================================
import socket
from os import path
from API_global_config import rest_rs


# ======================================================================================================================
# Readiness
# ======================================================================================================================
def readiness() -> (dict, int):
    """
    Readiness

    Proxy Socket check

    :return: dict, int
    """
    response, basename = dict(), path.basename(__file__)

    # ==================================================================================================================
    # Proxy Socket check
    # ==================================================================================================================
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(rest_rs.get("timeout"))
        result = sock.connect_ex((rest_rs.get("host"), rest_rs.get("port")))

    except Exception as err:
        response["error"] = f"{basename}: Proxy Socket check, check if Proxy is running, " \
                            f"error: {err}"

        return response, 500

    if result == 0:
        response["response"] = f"Proxy Socket check: port open, Proxy is running"

        return response, 200

    else:
        response["error"] = f"Proxy Socket check: port not open, check if Proxy running"

        return response, 500


# ======================================================================================================================
# End
# ======================================================================================================================
