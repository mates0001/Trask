###
# sudo docker build -t collector_api:latest .
###
FROM python:3.8-slim
MAINTAINER Ludek Matousek "ludekmatousek@email.cz"

WORKDIR /app
COPY . /app

# Automated copy of ORIGIN config file to avoid development user/pass creds to be part of the image build
# COPY API_global_config_ORIGIN.py /app/API_global_config.py

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["API_run.py"]