### UberServer homework

#### Purpose
[Original assignment](README_doc/assignment.txt)        
     
#### Table of content <a name="top"></a>
* [Prerequisites](#prerequisites)
* [Installation of Python virtual environment](#installation_of_python_virtual_environment)
* [Setup local development environment by docker-compose](#setup_local_development_environment_by_docker-compose)
* [API routes](#api_routes)    


#### Prerequisites <a name="prerequisites"></a>
* Python3
* Python virtual environment
* Python package manager "pip"

&nbsp; *Currently used CentOS7 with SCL based Python 3.8 & venv*  
&nbsp; *[Python venv requirements.txt](requirements.txt)*

[top](#top)

       
#### Installation of Python virtual environment <a name="installation_of_python_virtual_environment"></a>
Step 1: Clone Git repository
```
$ git clone <repository>
```
Step 2: Change into the project directory
```
$ cd <directory>
```
Step 3: Create Python virtual environment
```
$ python -m venv venv
```
Step 4: Activate Python virtual environment
```
$ source venv/bin/activate
```
Step 5: Install packages from requirements.txt file
```
(venv) $  pip install -r requirements.txt
```
Step 6: Deactivate Python virtual environment
```
$ deactivate
```

[top](#top)

     
#### Setup local development environment by docker-compose <a name="setup_local_development_environment_by_docker-compose"></a>
*Only for development Dev/Test purpose due to the temporary unsecure passwords, HTTP without SSL, etc.*           

Step 1: Bring local test environment up in detached mode
```
$ sudo docker-compose up -d
```
Step 2: Check state of containers
```
$ sudo docker-compose ps -a
```
Step 3: Check aggregated logs
```
$ sudo docker-compose logs -f
```
Step 4: Use Adminer to create MySQL database and insert "user data" as prerequisites for API RBAC      
&nbsp; API controlled exit in case of database and "user data" missing:
```
sudo $ docker-compose up -d
api               | 2022-04-20-10-26-30  WARNING     MYSQL.py, method: connect, ATTEMPT 1 - error: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
api               | 2022-04-20-10-26-35  WARNING     MYSQL.py, method: connect, ATTEMPT 2 - error: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
api               | 2022-04-20-10-26-40  ERROR       MYSQL.py, method: connect, error: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
api               | 2022-04-20-10-26-40  ERROR       AUTHENTICATION.py, method: Init, MySQL select user, error: MYSQL.py, method: connect, error: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
```
[Adminer http://0.0.0.0:8080](http://0.0.0.0:8080)      
*Temporary LocalStack Development user: root password: rootpassword*     
*SQL command: create database exam*      
![mysql1](README_doc/mysql1.PNG)    
[MySQL users.sql export file](users.sql)     
![mysql2](README_doc/mysql2.PNG)
     
Step 1: Bring remaining API container up
```
$ sudo docker start api
```
or
```
$ sudo docker-cpompose up -d
```
    
[top](#top)
       
      
###### API routes <a name="api_routes"></a>

**HEALTH**     
*liveness: simple hello*   
```
curl --request GET http://0.0.0.0:999/health/liveness
```
*readiness: check of Proxy port open/listen as Random Server hidden behind the proxy*    
```
curl --request GET http://0.0.0.0:999/health/readiness
```
**V1**    
*now: datetime in ISO format*       
&nbsp; *user role "user" & "vip"*         
```
curl --request GET --user user1:exam1 http://0.0.0.0:9999/v1/now
```
*VIP: VIP person location in point of time*   
&nbsp; *user role "vip"*               
```
curl --request GET --user user2:exam2 http://0.0.0.0:9999/v1/VIP/<int>
```

[top](#top)      
       
             